var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var config = require('./config/config');
var glob = require('glob');
var passport = require('passport');
var bodyParser = require('body-parser');
var cors = require('cors');
var compress = require('compression');
var methodOverride = require('method-override');

var app = express();
mongoose.connect(config.db,{ useNewUrlParser: true });
var db = mongoose.connection;
mongoose.set('debug', true);
db.on('error', function () {
    throw new Error('unable to connect to database at ' + config.db);
});

var models = glob.sync(config.root + '/models/*.js');
models.forEach(function (model) {
    require(model);
});




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(compress());
app.use(express.static(config.root + '/public'));
app.use(methodOverride());



require('./services/PassportService.js')(passport);



var controllers = glob.sync(config.root + '/controllers/*.js');
controllers.forEach(function (controller) {
    require(controller);
});

var routes = glob.sync(config.root + '/routes/*.js');
routes.forEach(function (route) {
    require(route)(app, passport);
});





// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});




// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'production' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});



app.listen(config.port, function () {
    console.log('Express server listening on port ' + config.port);
});



var out = '';

for (var row = 0; row < 9; row++) {
    for (var col = 0; col < 9; col++) {
        out += [""," "," ","  "," "," ","  "," "," "][col];
        out += printcode(board[sudoku.posfor(row, col)]);
    }
    out += ['\n','\n','\n\n','\n','\n','\n\n','\n','\n','\n'][row];
}

console.log(out);

module.exports = app;
