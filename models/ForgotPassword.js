var mongoose = require('mongoose'),
  Schema     = mongoose.Schema,
  crypto     = require('crypto'),
  ObjectId   = Schema.Types.ObjectId,

  ForgotPasswordSchema = new Schema({
    user:{type: ObjectId,ref:'User'},
    verificationCode: {type: Number}
  });

module.exports = mongoose.model('ForgotPassword', ForgotPasswordSchema);
