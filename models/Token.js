var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.Types.ObjectId;


var TokenSchema = new Schema({
    userId: {type: ObjectId, ref: 'User'},
    token: {type: String, unique: true},
    createdOn:{type: Date, default: Date.now},
});


mongoose.model('Token', TokenSchema);

