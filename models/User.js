var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs'),
    ObjectId = Schema.Types.ObjectId;


var UserSchema = new Schema({
    name: {type: String},
    email: {type: String, unique: true, trim: true},
    password:{type: String}
});
//Generating a hash
UserSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
};

// checking if password is valid
UserSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

mongoose.model('User', UserSchema);
