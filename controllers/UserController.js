var configCallback = require('../config/responseCallback');
var UserService = require('../services/UserService');
var TokenService = require('../services/TokenService');
var passport = require('passport');

exports.userRegister = function (req, res, next) {
    var responseCallback = configCallback.constructCallback(req, res);
    var callback = function (err, data, message) {
        if (err) {
            return responseCallback(err);
        } else {
            return responseCallback(null, {
                message: message,
                data: data,
                statusCode: 200,
                status: 'success'
            });
        }
    };
    passport.authenticate('user-registration', {session: false}, function (err, user, info) {
        if (user) {
            return responseCallback(null,{message:"User has been registered successfully."});
        } else if (err) {
            callback(err);
        } else if (info) {
            callback({
                message: info.message,
                title: 'Something went wrong!'
            }, null);
        }
    })(req, res, next);
};


exports.userLogin = function (req, res, next) {
    var responseCallback = configCallback.constructCallback(req, res);
    var callback = function (err, data, message) {
        if (err) {
            return responseCallback(err);
        } else {
            return responseCallback(null, {
                message: message,
                data: data,
                statusCode: 200,
                status: 'success'
            });
        }
    };
    passport.authenticate('user-login', {session: false}, function (err, user, info) {
        if (user) {
            TokenService.GenerateTokenForUser(user, callback);
        } else if (err) {
            callback(err);
        } else if (info) {
            callback({
                message: info.message,
                title: 'Something went wrong!'
            }, null);
        }
    })(req, res, next);
};

exports.forgotPassword = function (req, res, next) {
    var responseCallback = configCallback.constructCallback(req, res);
    var email = req.body.email;
    var callback = function (err, data, message) {
        if (err) {
            return responseCallback(err);
        } else {
            return responseCallback(null, {
                message: 'Note down the verification code to change the password.',
                data:{
                    verificationCode : data.verificationCode
                },
                statusCode: 200,
                status: 'success'
            });
        }
    };
    if(email){
        UserService.forgotPassword(email,callback);
    }else{
        return callback({message:"Email is required."});
    }
};

exports.changePassword = function (req, res, next) {
    var responseCallback = configCallback.constructCallback(req, res);
    var params = req.body;
    var callback = function (err, data, message) {
        if (err) {
            return responseCallback(err);
        } else {
            return responseCallback(null, {
                message: message,
                data: data,
                statusCode: 200,
                status: 'success'
            });
        }
    };
    if(params.oldPassword == params.newPassword){
        return callback({message:"The old and new password must be different."});
    }else{
        UserService.changePassword(params,callback);
    }
}

exports.resetPasswordUsingVerification = function (req, res, next) {
    var responseCallback = configCallback.constructCallback(req, res);
    var params = req.body;
    var callback = function (err, data, message) {
        if (err) {
            return responseCallback(err);
        } else {
            return responseCallback(null, {
                message: message,
                data: data,
                statusCode: 200,
                status: 'success'
            });
        }
    };
    if(params.code & params.password){
        UserService.resetPasswordUsingVerification(params,callback);
    }else{
        return callback({message: 'Invalid Parameters'});
    }

}