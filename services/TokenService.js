/**
 * Created by Z51 on 11/26/2016.
 */


var mongoose = require('mongoose');
var uuid = require('uuid').v4;
var Token = mongoose.model('Token');
var async = require('async');


module.exports = {
    GenerateTokenForUser: function (user, callback) {
        var generatedToken = uuid();
        async.waterfall([
                function (cb) {
            console.log("User",user);
                    Token.findOneAndRemove({userId: user._id}, cb)
                },
                function (token, cb) {
                    Token.create({
                        userId: user._id,
                        token: generatedToken
                    }, cb);
                }
            ],
            function (err, result) {
                var returnResult = {
                    token: result.token
                };
                callback(err, returnResult);
            });
    }
}
