var LocalStrategy = require('passport-local').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Token = mongoose.model('Token');

module.exports = function (passport) {

    passport.use('user-auth',
        new BearerStrategy(
            function (token, done) {
                Token.findOne({token: token})
                    .populate('userId')
                    .exec(function (err, user) {
                            if (err) {
                                return done(err)
                            }
                            if (!user) {
                                return done(null, false)
                            }
                            return done(null, user.userId, {scope: 'all'})
                        }
                    );
            }
        )
    );

    passport.use('user-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, email, password, done) {
            // we are checking to see if the user trying to login already exists
            User.findOne({'email': email}, function (err, user) {

                if (err)
                    return done(err);
                // if no user is found, return the message
                if (!user) {
                    return done(null, false, {message: 'User does not exist. Please create account first!'})
                }
                if (user) {
                    if (!user.validPassword(password)) {
                        return done(null, false, {'message': 'Oops! Wrong password.'});
                    } else {
                        // all is well, return successful user
                        return done(null, user);
                    }
                }
            })
        }
    ));

    passport.use('user-registration', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, email, password, done) {
            // we are checking to see if the user trying to login already exists
            User.findOne({'email': email.toLowerCase()}, function (err, user) {

                if (err)
                    return done(err);
                // if no user is found, return the message
                if (!user) {
                    var UserObj = new User({
                        email: email,
                        name: req.body.name
                    });
                    UserObj.password = UserObj.generateHash(password);
                    UserObj.save(done);
                }
                if (user) {
                    return done(null, false, {message: 'User already registered. Please login!'})
                }
            })
        }
    ));
}
