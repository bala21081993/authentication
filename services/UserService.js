var mongoose = require('mongoose');
var User = mongoose.model('User');
var ForgotPassword = mongoose.model('ForgotPassword');

module.exports = {
    forgotPassword: function(email, callback){
        User.findOne({email: email}, function(err,user){
            if(user){
                var getRandomDigits = Math.floor(Math.random() * (Math.pow(10,5) * 9)) + Math.pow(10,5);
                var verificationDigit = getRandomDigits;
                ForgotPassword.create({user: user._id, verificationCode: verificationDigit}, function (err, fPass) {
                    if (fPass) {
                        return callback(null, fPass);
                    } else if (err) {
                        return callback(err);
                    } else {
                        return callback({msg: "Error in creating verification digit.", code: 404});
                    }
                });
            }else{
                return callback({msg: "There is no user in this email id.", code: 404});
            }
        })
    },
    changePassword: function(params, callback){
        User.findOne({email: params.email}, function(err,user){
            if(user){
                if(user.validPassword(params.oldPassword)){
                    user.password = user.generateHash(params.newPassword);
                    user.save(function(err,doc){
                        if(err){
                            throw err;
                        }
                        callback(null,{message:"Password has ben changed successfully."});

                    })
                }else{
                    return callback({'message': 'Invalid password.'});
                }
            }else{
                return callback({msg: "There is no user in this email id.", code: 404});
            }
        })
    },
    resetPasswordUsingVerification: function(params, callback){
        ForgotPassword.findOne({verificationCode:params.code}, function(err,doc){
            if(doc){
                console.log("doc...",doc);
                User.findOne({_id:doc.user},function(err,doc){
                    doc.password = doc.generateHash(params.password);
                    doc.save(function(err,result){
                        if(err){
                            throw err;
                        }
                        callback(null,{message:"Password has ben changed successfully."});

                    })
                });
            }else{
                callback({message: 'Invalid verification code.!'});
            }
        })
    }
}