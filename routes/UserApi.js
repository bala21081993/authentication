var controller = require('../controllers/UserController');
var config = require('../config/config');

module.exports = function (app, passport) {

    app.post('/user/register', controller.userRegister);
    app.post('/user/login', controller.userLogin);
    app.post('/user/resetPassword', controller.forgotPassword);
    app.post('/user/changePassword', passport.authenticate('user-auth', {session: false}),controller.changePassword);
    app.post('/user/updatePasswordUsingVerification', controller.resetPasswordUsingVerification);

};