module.exports = {
    createSuccessObj: function (data) {
        var returnObj = {};
        if (data.status) {
            returnObj.status = data.status;
        }
        if (data.data !== undefined && data.data !== null) {
            returnObj.data = data.data;
        }
        if (data.message) {
            returnObj.message = data.message;
        }
        return returnObj;
    },
    createErrorObj: function (err) {
        var returnObj = {
            status: 'error'
        };
        if (err.message) {
            returnObj.message = err.message;
        }
        if (err.title) {
            returnObj.title = err.title;
        }
        if (err.code) {
            returnObj.code = err.code;
        }
        return returnObj;
    },
    constructCallback: function (req, res) {
        var self = this;
        return function (err, data) {
            if (err) {
                res.status(err.status || 500);
                var errorObj = self.createErrorObj(err);
                res.json(errorObj);
            } else {
                res.status(data.statusCode || 200);
                var successObj = self.createSuccessObj(data);
                res.json(successObj);
            }
        };
    }
};