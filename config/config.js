var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'production';

var config = {
    production: {
        root: rootPath,
        app: {
            name: 'test-apis'
        },
        port: process.env.PORT || 3000,
        db: 'mongodb://localhost/authentication',
    }
};

module.exports = config[env];
